#ifndef ARRAY_H
#define ARRAY_H

/**
 * @brief Structure du tableau
 */
typedef struct array* Array;

/**
 * @brief Type du tableau
 */
typedef int data_type;

/**
 * @brief Créer la structure tableau
 * 
 * @param capacity - Capacité du tableau à créer
 * @return Array - Tableau créé
 */
Array array_create (int capacity);

/**
 * @brief Libère l'espace mémoire du tableau
 * 
 * @param table - Le tableau
 */
void array_destroy (Array table);

/**
 * @brief Copie le tableau
 * 
 * @param table - Le tableau
 * @return Array - Copie du tableau
 */
Array array_copy (Array table);

/**
 * @brief Donne la taille actuelle du tableau
 * 
 * @param table - Le tableau
 * @return int - Taille du tableau
 */
int array_size (Array table);

/**
 * @brief Donne la capacité du tableau
 * 
 * @param table - Le tableau
 * @return int - Capacité du tableau
 */
int array_capacity (Array table);

/**
 * @brief Test si le tableau est vide
 * 
 * @param table - Le tableau
 * @return bool - Vrai si le tableau est vide, faux sinon
 */

/**
 * @brief Teste si le tableau est vide
 * 
 * @param table - Le tableau
 * @return true - Si le tableau est vide
 * @return false - Si le tableau n'est pas vide
 */
bool array_is_empty (Array table);

/**
 * @brief Ajoute un élément à la fin du tableau
 * 
 * @param table - Le tableau
 * @param data - Elément à ajouter
 */
void array_append (Array table, data_type data);

/**
 * @brief Ajoute un élément à la position donnée
 * 
 * @param table - Le tableau
 * @param data - Elément à ajouter
 * @param indice - Position où insérer le nouvel élément
 */
void array_append_at (Array table, data_type data, int indice);

/**
 * @biref Remplace un élément par un autre
 * 
 * @param table - Le tableau
 * @param data - Elément de remplacement
 * @param indice - Position de l'élément à remplacer
 */
void array_replace_at (Array table, data_type data, int indice);

/**
 * @brief Donne l'élément à la position donnée
 * 
 * @param table - Le tableau
 * @param indice - Position de l'élément
 * @return data_type - L'élément concerné
 */
data_type array_value_at (Array table, int indice);

/**
 * @brief Supprime le dernier élément du tableau
 * 
 * @param table - Le tableau
 */
void array_delete_last (Array table);

/**
 * @brief Supprime l'élément à la position donnée
 * 
 * @param table - Le tableau
 * @param indice - La position de l'élément à supprimer
 */
void array_delete_at (Array table, int indice);

/**
 * @brief Supprime toutes les occurences de l'élément donné
 * 
 * @param table - Le tableau
 * @param data - L'élément à supprimer
 */
void array_delete_all_occurences (Array table, data_type data);

/**
 * @brief Vide le tableau
 * 
 * @param table - Le tableau
 */
void array_delete_all(Array table);

/**
 * @brief Décale les éléments du tableau
 * 
 * @param table - Le tableau
 * @param start - Position du début du décalage
 * @param end - Position de la fin du décalage
 * @param shift - Le nombre de décalage (>0 : droite, <0 : gauche)
 */
void array_shift (Array table, int start, int end, int shift);

/**
 * @brief Fusionne table_1 et table_2
 * 
 * @param table_1 - Le tableau 1
 * @param table_2 - Le tableau 2
 * @return Array - La fusion de table_1 et table_2
 */
Array array_merge (Array table_1, Array table_2);

/**
 * @brief Concatenne table_1 et table_2
 * 
 * @param table_1 - Le tableau 1
 * @param table_2 - Le tableau 2
 * @return Array - La concatennation de table_1 et table_2
 */
Array array_concat (Array table_1, Array table_2);

/**
 * @brief Trie le tableau
 * 
 * @param table - Le tableau
 */
void array_sort (Array table);

/**
 * @brief Recherche un élément dans le tableau
 * 
 * @param table - Le tableau
 * @param data - L'élément à rechercher
 * @return int - La position de l'élément recherché (-1 dans le cas echeant)
 */
int array_search (Array table, data_type data);

/**
 * @brief Recherche un élément dans le tableau trie
 * 
 * @param table - Le tableau trié
 * @param data - L'élément à rechercher
 * @return int - La position de la élément recherchée (-1 dans le cas echeant)
 */
int array_dsearch (Array table, data_type data);

/**
 * @brief Affiche le tableau
 * 
 * @param table - Le tableau
 * @param pattern - Template d'affichage pour printf (ex: "%3d -> %5d")
 * @param column - Le nombre de colonnes pour l'affichage 
 */
void array_print (Array table, char* pattern, int column);

#endif /* ARRAY_H */
