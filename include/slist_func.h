#ifndef SLIST_FUNC_H
#define SLIST_FUNC_H

#include "slist.h"

/**
 * @brief Supprime toutes les occurences de data dans la liste
 * 
 * @param list - La liste
 * @param data - Le ou les élément(s) à supprimer
 * @return SList - La liste sans les occurences de data
 */
SList slist_delete_all_occurences (SList list, data_type data);

/**
 * @brief Renvoie la longueur de la liste
 * 
 * @param list - La liste
 * @return int - La longueur de la liste
 */
int slist_length (SList list);

/**
 * @brief Copie la liste
 * 
 * @param list - La liste
 * @return SList - La copie de la liste
 */
SList slist_copy (SList list);

/**
 * @brief Ajoute un élément en fin de liste
 * 
 * @param list - La liste
 * @param data - L'élément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
SList slist_append (SList list, data_type data);

/**
 * @brief Renverse la liste
 * 
 * @param list - La liste
 * @return SList - La liste reversée
 */
SList slist_reverse (SList list);

/**
 * @brief Concatène list_1 avec list_2
 * 
 * @param list_1 - La liste 1
 * @param list_2 - La liste 2
 * @return SList - Le résultat de la concatenation de list_1 et list_2
 */
SList slist_concat (SList list_1, SList list_2);

/**
 * @brief Renvoie un poiteur sur le dernier élément de la liste
 * 
 * @param list - La liste
 * @return SList - Poiteur sur le dernier élément
 */
SList slist_last (SList list);

/**
 * @brief Renvoie un poiteur sur le n ième élément de la liste
 * 
 * @param list - La liste
 * @param n - Numéro de la cellule concernée
 * @return SList - Pointeur sur le n ième élément de la liste
 */
SList slist_nth (SList list, int n);

/**
 * @brief Renvoie le n ième élément de la liste
 * 
 * @param list - La liste
 * @param n - Numéro de la cellule concernée
 * @return data_type - Element de la n ième cellule de la liste
 */
data_type slist_nth_data (SList list, int n);

/**
 * @brief Recherche un élément dans la liste
 * 
 * @param list - La liste
 * @param data - Elément recherché
 * @return SList - Pointeur sur la cellule recherchée, renvoie NULL si non trouvé
 */
SList slist_find (SList list, data_type data);

/**
 * @brief Recherche une cellule de la liste
 * 
 * @param list - La liste
 * @param p - Poiteur recherché
 * @return int - Position du pointeur dans la liste, renvoie -1 si non trouvé
 */
int slist_position (SList list, SList p);

/**
 * @brief Recherche un élément dans la liste
 * 
 * @param list - La liste
 * @param data - L'élément recherché
 * @return int - Indice de l'élément recherché dans la liste, renvoie -1 si non trouvé
 */
int slist_index (SList list, data_type data);

/**
 * @brief Affiche la liste
 * 
 * @param list - La liste
 * @param pattern - Template d'affichage pour printf (ex: "%d, ")
 */
void slist_print (SList list, char* pattern);

#endif /* SLIST_FUNC_H */
