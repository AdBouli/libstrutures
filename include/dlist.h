#ifndef SLIST_H
#define SLIST_H

/**
 * @brief Structure de la liste
 */
typedef struct dlist* DList;

/**
 * @brief Type de la liste
 */
typedef int data_type;

/**
 * @brief Créer la structure liste
 * 
 * @return SList - Liste créée
 */
DList dlist_create ();

/**
 * @brief Libère l'espace mémoire d'une cellule de la liste
 * 
 * @param list - La liste
 */
void dlist_free ();

/**
 * @brief Libère l'espace mémoire de la liste
 * 
 * @param list - La liste
 */
void dlist_destroy ();

/**
 * @brief Ajoute une valeur en début de liste
 * 
 * @param list - La liste
 * @param data - Elément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
DList dlist_prepend (DList list, data_type data);

/**
 * @brief Supprime le premier élément de la liste
 * 
 * @param list - La liste
 * @return SList - La liste sans le premier élément
 */
DList dlist_delete_first (DList list);

/**
 * @brief Ajoute l'élément après la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @param data - Elément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
DList dlist_insert_after (DList list, DList p, data_type data);

/**
 * @brief Supprime l'élément après la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @return SList - La liste sans l'élément après la cellule p
 */
DList dlist_delete_after (DList list, DList p);

/**
 * @brief Ajoute l'élément avant la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @param data - Elément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
DList dlsit_insert_before (DList list, DList p, data_type data);

/**
 * @brief Supprime l'élément avant la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @return SList - La liste sans l'élément après la cellule p
 */
DList dlist_delete_before (DList list, DList p);

/**
 * @brief Renvoie un pointeur sur la cellule suivante de la liste
 * 
 * @param list - La liste
 * @return SList - Pointeur sur la suite de la liste
 */
DList dlist_next (DList list);

/**
 * @brief Renvoie un pointeur sur la cellule précédante de la liste
 * 
 * @param list - La liste
 * @return SList - Pointeur sur la cellule précédante de la liste
 */
DList dlist_prev (DList list);

/**
 * @brief Renvoie l'élément de la première case de la liste
 * 
 * @param list - La liste
 * @return data_type - L'élément de la première case
 */
data_type dlist_data (DList list);

/**
 * @brief Teste si la liste est vide
 * 
 * @param list - La liste
 * @return true - Si la liste est vide
 * @return false - Si la liste n'est pas vide
 */
bool dlist_is_empty (DList list);

#endif /* SLIST_H */
