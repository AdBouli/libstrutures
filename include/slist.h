#ifndef SLIST_H
#define SLIST_H

/**
 * @brief Structure de la liste
 */
typedef struct slist* SList;

/**
 * @brief Type de la liste
 */
typedef int data_type;

/**
 * @brief Créer la structure liste
 * 
 * @return SList - Liste créée
 */
SList slist_create ();

/**
 * @brief Libère l'espace mémoire d'une cellule de la liste
 * 
 * @param list - La liste
 */
void slist_free (SList list);

/**
 * @brief Libère l'espace mémoire de la liste
 * 
 * @param list - La liste
 */
void slist_destroy (SList list);

/**
 * @brief Ajoute une valeur en début de liste
 * 
 * @param list - La liste
 * @param data - Elément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
SList slist_prepend (SList list, data_type data);

/**
 * @brief Supprime le premier élément de la liste
 * 
 * @param list - La liste
 * @return SList - La liste sans le premier élément
 */
SList slist_delete_first (SList list);

/**
 * @brief Ajoute l'élément après la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @param data - Elément à ajouter
 * @return SList - La liste avec le nouvel élément
 */
SList slist_insert_after (SList list, SList p, data_type data);

/**
 * @brief Supprime l'élément après la cellule p dans la liste
 * 
 * @param list - La liste
 * @param p - Pointeur sur la cellule concernée
 * @return SList - La liste sans l'élément après la cellule  p
 */
SList slist_delete_after (SList list, SList p);

/**
 * @brief Renvoie un pointeur sur la cellule suivante de la liste
 * 
 * @param list - La liste
 * @return SList - Pointeur sur la suite de la liste
 */
SList slist_next (SList list);

/**
 * @brief Renvoie l'élément de la première cellule de la liste
 * 
 * @param list - La liste
 * @return data_type - L'élément de la première cellule
 */
data_type slist_data (SList list);

/**
 * @brief Teste si la liste est vide
 * 
 * @param list - La liste
 * @return true - Si la liste est vide
 * @return false - Si la liste n'est pas vide
 */
bool slist_is_empty (SList list);

#endif /* SLIST_H */
